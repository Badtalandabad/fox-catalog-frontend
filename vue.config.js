module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    port: 3040,
    host: '192.168.2.2',
    watchOptions: {
      poll: true
    },
  }
}
