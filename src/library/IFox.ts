interface IFox {
    id: number,
    name: string,
    image: string,
    article: string,
}

export default IFox