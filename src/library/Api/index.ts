import ApiResponse from "./ApiResponse"
import IFox from "@/library/IFox"

class Api {
  public readonly BASE_URL = 'http://foxcatalog.local/api/v1/'
  public readonly GET_FOXES_ACTION = 'get-foxes'

  /**
   * Does request to API
   *
   * @param actionName Constant, action name
   * @param data       Data to send to server (empty object if no data needed)
   */
  public callApi<REQ_T>(actionName: string, data: REQ_T): Promise<ApiResponse<Record<string, unknown>>> {
    const endpoint = this.BASE_URL + actionName

    const params = {
      method: 'GET',
      mode: 'cors' as RequestMode,
      headers: { 'Accept': 'application/json', },
      //body: JSON.stringify(data),
    }

    return fetch(endpoint, params).then((response) => {
      if (!response.ok) {
        throw new Error('API responded with an error')
      }

      return response.json().then(resultObj => {
        if (typeof resultObj.message !== 'string'
          || typeof resultObj.data !== 'object'
        ) {
          throw new Error('Response format is incorrect')
        }

        return new ApiResponse(response.status, resultObj.message, resultObj.data)
      })
    })
  }

  public getFoxes(): Promise<IFox[]> {
    return this.callApi(this.GET_FOXES_ACTION, null)
      .then(result => {
        if (!Array.isArray(result.data?.foxes)) {
          throw new Error('Response format from get-foxes request is incorrect')
        }

        //TODO: instantiate Fox class
        return result.data.foxes as IFox[]
      });
  }
}

export default new Api();